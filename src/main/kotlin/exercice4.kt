interface Question {
    fun buildQuestion(numOfQuestion: Int)
    fun checkSolvedQuestion(): Boolean
}

class Quiz {
    val listOfQuestion = mutableListOf<Question>()
    init {
        listOfQuestion.add(MultipleChoiceQuestion("¿Cuál es el porcentaje de agua que ocupa el organismo del ser humano?", listOf("60%", "55%", "76%", "68%"), "60%"))
        listOfQuestion.add(FreeTextQuestion("¿Cuanto es (2+6)/2 ?","4"))
        listOfQuestion.add(MultipleChoiceQuestion("¿Cuál es la capital de Mongolia?", listOf("Dublin", "Madrid", "Ulan Bator", "Ginea"), "Ulan Bator"))
        listOfQuestion.add(FreeTextQuestion("¿Cuál es el país más poblado del mundo?","China"))
    }
}

class FreeTextQuestion(private val title: String, private val answer: String): Question {
    private var solved = false

    override fun buildQuestion(numOfQuestion: Int) {
        println("Pregunta: ${numOfQuestion + 1} (FreeText)")
        println(title)
    }

    override fun checkSolvedQuestion(): Boolean {
        print("Indica aqui tu respuesta: ")
        val userInput = sc.nextLine()
        if (userInput.lowercase() == answer.lowercase()){
            solved = true
            println("\nRespuesta correcta\n")
        } else println("\nRespuesta incorrecta\n")
        return solved
    }
}

class MultipleChoiceQuestion(private val title: String, private val choices: List<String>, private val answer: String): Question {
    private var solved = false

    override fun buildQuestion(numOfQuestion: Int) {
        println("Pregunta: ${numOfQuestion + 1} (MultipleChoice)")
        println(title)
        for (i in choices.indices){
            print("Opcion ${i+1}: ${choices[i]}\n")
        }
    }

    override fun checkSolvedQuestion(): Boolean {
        print("\nIndica aqui tu respuesta: ")
        val userInput = sc.nextLine()
        if (userInput.lowercase() == answer.lowercase()){
            solved = true
            println("\nRespuesta correcta\n")
        } else println("\nRespuesta incorrecta\n")
        return solved
    }
}

fun main() {
    val listOfQuestion = Quiz().listOfQuestion
    val rightQuestion = mutableListOf<String>()

    for (i in listOfQuestion.indices){
        listOfQuestion[i].buildQuestion(i)
        if (listOfQuestion[i].checkSolvedQuestion()) rightQuestion.add("Problema ${i+1}: Acertado")
        else rightQuestion.add("Problema ${i + 1}: Fallado")
    }

    println("Estos son los problemas acertados:")
    for (i in rightQuestion.indices){
        println(rightQuestion[i])
    }
}