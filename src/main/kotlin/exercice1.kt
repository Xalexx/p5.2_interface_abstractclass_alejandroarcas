enum class Notas{
    SUSPENDIDO, APROBADO, BIEN, NOTABLE, EXCELENTE
}

data class Estudiante(val nombre: String, val nota: Notas)

fun main() {
    val firstStudent = Estudiante("Alberto", Notas.values().random())
    val secondStudent = Estudiante("Luis", Notas.values().random())

    println("$firstStudent\n$secondStudent")
}