import java.util.Scanner

enum class Direction {
    FRONT, LEFT, RIGHT
}

interface CarSensors {
    fun isThereSomethingAt(direction: Direction): Boolean
    fun go(direction: Direction)
    fun stop()
}

class AutonomousCar: CarSensors {

    override fun isThereSomethingAt(direction: Direction): Boolean {
        println("Hay algun obstaculo en la siguiente direccion: $direction?")
        return sc.nextBoolean()
    }

    override fun go(direction: Direction) {
        println("Avanzando a la siguiente direccion: $direction")
    }

    override fun stop() {
        println("El coche se ha parado, porque no puede avanzar a ninguna direccion")
    }

    fun doNextNSteps(n :Int){
        for (i in 1..n){
            if (isThereSomethingAt(Direction.FRONT)){
                if (isThereSomethingAt(Direction.RIGHT)){
                    if (isThereSomethingAt(Direction.LEFT)){
                        stop()
                    } else go(Direction.LEFT)
                } else go(Direction.RIGHT)
            } else go(Direction.FRONT)
        }
    }
}

val sc = Scanner(System.`in`)

fun main() {
    val autonomousCar = AutonomousCar()
    autonomousCar.doNextNSteps(7)
}