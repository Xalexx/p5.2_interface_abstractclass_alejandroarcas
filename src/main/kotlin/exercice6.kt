import java.util.*

interface GymControlReader {
    fun nextId(): String
}

class GymControlManualReader(val sc : Scanner = Scanner(System.`in`)): GymControlReader {
    override fun nextId(): String = sc.next()
}

fun main() {
    val gymMenuReader = GymControlManualReader()
    val gymUsersList = mutableListOf<String>()

    for (i in 1..8){
        val gymId = gymMenuReader.nextId()
        if (gymId in gymUsersList){
            println("$gymId Sortida")
            gymUsersList.remove(gymId)
        } else {
            println("$gymId Entrada")
            gymUsersList.add(gymId)
        }
    }
}