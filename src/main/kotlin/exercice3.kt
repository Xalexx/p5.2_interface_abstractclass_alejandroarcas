interface Instrument{
    var sound: String
    fun makeSounds(numberOfSound: Int)
}

class Drum(override var sound: String): Instrument{
    init {
        when (sound){
            "A" -> sound = "TAAAM"
            "O" -> sound = "TOOOM"
            "U" -> sound = "TUUUM"
        }
    }
    override fun makeSounds(numberOfSound: Int) {
        repeat(numberOfSound){ println(sound) }
    }

}

class Triangle(resonance: Int, ): Instrument{
    override var sound: String = "T"
    init {
        for (i in 1..resonance){
            sound += "I"
        }
        sound += "NC"
    }
    override fun makeSounds(numberOfSound: Int) {
        repeat(numberOfSound){ println(sound) }
    }
}

fun main(args: Array<String>) {
    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Drum("A"),
        Drum("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)
}

private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        instrument.makeSounds(2) // plays 2 times the sound
    }
}