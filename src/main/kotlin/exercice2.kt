import java.util.Scanner

enum class Rainbow{
    RED, ORANGE, YELLOW, GREEN, BLUE, INDIGO, VIOLET
}

fun main() {
    val sc = Scanner(System.`in`)
    val userColor = sc.next()
    val listOfColors = mutableListOf<String>()

    for (i in Rainbow.values()){
        listOfColors.add(i.toString())
    }

    if (userColor.uppercase() in listOfColors) println(true)
    else println(false)
}